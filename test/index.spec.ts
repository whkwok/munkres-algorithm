import { minWeightAssign, maxWeightAssign } from 'src';

describe('minWeightAssign and maxWeightAssign', () => {
  describe('main', () => {
    it('should be able to compute assignments correctly (empty weightMatrix)', () => {
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign([]);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign([]);
      expect(minAssignments).toEqual([]);
      expect(minAssignmentsWeight).toEqual(0);
      expect(maxAssignments).toEqual([]);
      expect(maxAssignmentsWeight).toEqual(0);
    });

    it('should be able to compute assignments correctly (square weightMatrix (1,1))', () => {
      const weightMatrix = [[1000]];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([0]);
      expect(minAssignmentsWeight).toEqual(1000);
      expect(maxAssignments).toEqual([0]);
      expect(maxAssignmentsWeight).toEqual(1000);
    });

    it('should be able to compute assignments correctly (square weightMatrix (3,3))', () => {
      const weightMatrix = [
        [400, 150, 400],
        [400, 450, 600],
        [300, 225, 300],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([1, 0, 2]);
      expect(minAssignmentsWeight).toEqual(150 + 400 + 300);
      expect(maxAssignments).toEqual([0, 2, 1]);
      expect(maxAssignmentsWeight).toEqual(400 + 600 + 225);
    });

    it('should be able to compute assignments correctly (square weightMatrix (4,4))', () => {
      const weightMatrix = [
        [0, 0, 0, 0],
        [0, 1, 2, 3],
        [0, 2, 4, 6],
        [0, 3, 6, 9],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([3, 2, 1, 0]);
      expect(minAssignmentsWeight).toEqual(0 + 2 + 2 + 0);
      expect(maxAssignments).toEqual([0, 1, 2, 3]);
      expect(maxAssignmentsWeight).toEqual(0 + 1 + 4 + 9);
    });

    it('should be able to compute assignments correctly (non-square weightMatrix (3,4))', () => {
      const weightMatrix = [
        [9, 0, 0, 9],
        [0, 1, 2, 3],
        [0, 2, 4, 6],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([2, 1, 0]);
      expect(minAssignmentsWeight).toEqual(0 + 1 + 0);
      expect(maxAssignments).toEqual([0, 2, 3]);
      expect(maxAssignmentsWeight).toEqual(9 + 2 + 6);
    });

    it('should be able to compute assignments correctly (non-square weightMatrix (4,3))', () => {
      const weightMatrix = [
        [9, 0, 0],
        [0, 1, 2],
        [0, 2, 4],
        [9, 3, 6],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([2, 1, 0, null]);
      expect(minAssignmentsWeight).toEqual(0 + 1 + 0);
      expect(maxAssignments).toEqual([0, null, 1, 2]);
      expect(maxAssignmentsWeight).toEqual(9 + 2 + 6);
    });

    it('should be able to compute assignments correctly (non-square weightMatrix (6,3))', () => {
      const weightMatrix = [
        [9, 0, 0],
        [0, 1, 2],
        [0, 2, 4],
        [9, 3, 6],
        [4, 1, 7],
        [0, 1, 0],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([1, 0, null, null, null, 2]);
      expect(minAssignmentsWeight).toEqual(0 + 0 + 0);
      expect(maxAssignments).toEqual([0, null, null, 1, 2, null]);
      expect(maxAssignmentsWeight).toEqual(9 + 3 + 7);
    });

    it('should be able to compute assignments correctly (all +Infinity weights)', () => {
      const weightMatrix = [
        [Infinity, Infinity, Infinity],
        [Infinity, Infinity, Infinity],
        [Infinity, Infinity, Infinity],
        [Infinity, Infinity, Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([null, null, null, null]);
      expect(minAssignmentsWeight).toEqual(0);
      expect(maxAssignments).toEqual([0, 1, 2, null]);
      expect(maxAssignmentsWeight).toEqual(Infinity);
    });

    it('should be able to compute assignments correctly (some +Infinity weights case 1)', () => {
      const weightMatrix = [
        [5, Infinity, Infinity],
        [6, Infinity, Infinity],
        [Infinity, Infinity, 10],
        [4, Infinity, Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([null, null, 2, 0]);
      expect(minAssignmentsWeight).toEqual(10 + 4);
      expect(maxAssignments).toEqual([1, 2, 0, null]);
      expect(maxAssignmentsWeight).toEqual(Infinity);
    });

    it('should be able to compute assignments correctly (some +Infinity weights case 2)', () => {
      const weightMatrix = [
        [5, 10, Infinity],
        [6, Infinity, Infinity],
        [Infinity, 0, 10],
        [4, Infinity, Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([1, null, 2, 0]);
      expect(minAssignmentsWeight).toEqual(10 + 10 + 4);
      expect(maxAssignments).toEqual([2, 1, 0, null]);
      expect(maxAssignmentsWeight).toEqual(Infinity);
    });
    it('should be able to compute assignments correctly (some +Infinity weights case 3)', () => {
      const weightMatrix = [
        [5, 10, Infinity],
        [6, Infinity, Infinity],
        [0, 0, 10],
        [4, Infinity, Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([1, null, 2, 0]);
      expect(minAssignmentsWeight).toEqual(10 + 10 + 4);
      expect(maxAssignments).toEqual([2, 0, null, 1]);
      expect(maxAssignmentsWeight).toEqual(Infinity);
    });

    it('should be able to compute assignments correctly (some +Infinity weights case 4)', () => {
      const weightMatrix = [
        [5, 10, 1],
        [6, Infinity, Infinity],
        [0, 0, 10],
        [4, 2, Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([2, null, 0, 1]);
      expect(minAssignmentsWeight).toEqual(1 + 0 + 2);
      expect(maxAssignments).toEqual([0, 1, null, 2]);
      expect(maxAssignmentsWeight).toEqual(Infinity);
    });

    it('should be able to compute assignments correctly (some +Infinity weights case 5)', () => {
      const weightMatrix = [
        [5, Infinity, Infinity],
        [6, Infinity, Infinity],
        [4, Infinity, Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([null, null, 0]);
      expect(minAssignmentsWeight).toEqual(4);
      expect(maxAssignments).toEqual([1, 0, 2]);
      expect(maxAssignmentsWeight).toEqual(Infinity);
    });

    it('should be able to compute assignments correctly (all -Infinity weights)', () => {
      const weightMatrix = [
        [-Infinity, -Infinity, -Infinity],
        [-Infinity, -Infinity, -Infinity],
        [-Infinity, -Infinity, -Infinity],
        [-Infinity, -Infinity, -Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([0, 1, 2, null]);
      expect(minAssignmentsWeight).toEqual(-Infinity);
      expect(maxAssignments).toEqual([null, null, null, null]);
      expect(maxAssignmentsWeight).toEqual(0);
    });

    it('should be able to compute assignments correctly (some -Infinity weights case 1)', () => {
      const weightMatrix = [
        [5, 0, 3],
        [6, -Infinity, 2],
        [-Infinity, 0, 10],
        [4, 10, -Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([null, 1, 0, 2]);
      expect(minAssignmentsWeight).toEqual(-Infinity);
      expect(maxAssignments).toEqual([null, 0, 2, 1]);
      expect(maxAssignmentsWeight).toEqual(6 + 10 + 10);
    });

    it('should be able to compute assignments correctly (some -Infinity weights case 2)', () => {
      const weightMatrix = [
        [5, 0, 3],
        [6, -Infinity, 2],
        [3, 0, 10],
        [4, 10, -Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([null, 1, 0, 2]);
      expect(minAssignmentsWeight).toEqual(-Infinity);
      expect(maxAssignments).toEqual([null, 0, 2, 1]);
      expect(maxAssignmentsWeight).toEqual(6 + 10 + 10);
    });

    it('should be able to compute assignments correctly (some -Infinity weights case 3)', () => {
      const weightMatrix = [
        [5, 0, 3],
        [6, -Infinity, 2],
        [3, 0, 10],
        [4, 10, 11],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([2, 1, 0, null]);
      expect(minAssignmentsWeight).toEqual(-Infinity);
      expect(maxAssignments).toEqual([null, 0, 2, 1]);
      expect(maxAssignmentsWeight).toEqual(6 + 10 + 10);
    });

    it('should be able to compute assignments correctly (some -Infinity weights case 4)', () => {
      const weightMatrix = [
        [5, 0, 3],
        [6, -Infinity, -Infinity],
        [3, 0, 10],
        [4, 10, 11],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([1, 2, 0, null]);
      expect(minAssignmentsWeight).toEqual(-Infinity);
      expect(maxAssignments).toEqual([null, 0, 2, 1]);
      expect(maxAssignmentsWeight).toEqual(6 + 10 + 10);
    });

    it('should be able to compute assignments correctly (some -Infinity weights case 5)', () => {
      const weightMatrix = [
        [5, -Infinity, -Infinity],
        [6, -Infinity, -Infinity],
        [4, -Infinity, -Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([1, 2, 0]);
      expect(minAssignmentsWeight).toEqual(-Infinity);
      expect(maxAssignments).toEqual([null, 0, null]);
      expect(maxAssignmentsWeight).toEqual(6);
    });

    it('should be able to compute assignments correctly (all Infinity and -Infinity weights)', () => {
      const weightMatrix = [
        [Infinity, Infinity, Infinity],
        [Infinity, -Infinity, -Infinity],
        [Infinity, -Infinity, Infinity],
        [-Infinity, Infinity, Infinity],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([null, 2, 1, 0]);
      expect(minAssignmentsWeight).toEqual(-Infinity);
      expect(maxAssignments).toEqual([0, null, 2, 1]);
      expect(maxAssignmentsWeight).toEqual(Infinity);
    });

    it('should be able to compute assignments correctly (some Infinity and -Infinity weights)', () => {
      const weightMatrix = [
        [5, Infinity, 3],
        [Infinity, -Infinity, -Infinity],
        [1, Infinity, Infinity],
        [Infinity, Infinity, 5],
      ];
      const {
        assignments: minAssignments,
        assignmentsWeight: minAssignmentsWeight,
      } = minWeightAssign(weightMatrix);
      const {
        assignments: maxAssignments,
        assignmentsWeight: maxAssignmentsWeight,
      } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([2, 1, 0, null]);
      expect(minAssignmentsWeight).toEqual(-Infinity);
      expect(maxAssignments).toEqual([1, 0, 2, null]);
      expect(maxAssignmentsWeight).toEqual(Infinity);
    });
  });

  describe('input', () => {
    it('should be able to support matrix (float32)', () => {
      const mWeightMatrix = {
        data: Float32Array.from([2, 1, 2, 1]),
        shape: [2, 2],
      };
      const aWeightMatrix = [
        [2, 1],
        [2, 1],
      ];
      const { assignments: mAssignments, assignmentsWeight: mAssignmentsCost } =
        minWeightAssign(mWeightMatrix);
      const { assignments: aAssignments, assignmentsWeight: aAssignmentsCost } =
        minWeightAssign(aWeightMatrix);
      expect(aAssignments).toEqual(mAssignments);
      expect(aAssignmentsCost).toEqual(mAssignmentsCost);
    });

    it('should be able to support matrix (float64)', () => {
      const mWeightMatrix = {
        data: Float64Array.from([2, 1, 2, 1]),
        shape: [2, 2],
      };
      const aWeightMatrix = [
        [2, 1],
        [2, 1],
      ];
      const { assignments: mAssignments, assignmentsWeight: mAssignmentsCost } =
        minWeightAssign(mWeightMatrix);
      const { assignments: aAssignments, assignmentsWeight: aAssignmentsCost } =
        minWeightAssign(aWeightMatrix);
      expect(aAssignments).toEqual(mAssignments);
      expect(aAssignmentsCost).toEqual(mAssignmentsCost);
    });

    it('should be able to support matrix (int8)', () => {
      const mWeightMatrix = {
        data: Float64Array.from([2, 1, 2, 1]),
        shape: [2, 2],
      };
      const aWeightMatrix = [
        [2, 1],
        [2, 1],
      ];
      const { assignments: mAssignments, assignmentsWeight: mAssignmentsCost } =
        minWeightAssign(mWeightMatrix);
      const { assignments: aAssignments, assignmentsWeight: aAssignmentsCost } =
        minWeightAssign(aWeightMatrix);
      expect(aAssignments).toEqual(mAssignments);
      expect(aAssignmentsCost).toEqual(mAssignmentsCost);
    });

    it('should be able to support matrix (int16)', () => {
      const mWeightMatrix = {
        data: Int16Array.from([2, 1, 2, 1]),
        shape: [2, 2],
      };
      const aWeightMatrix = [
        [2, 1],
        [2, 1],
      ];
      const { assignments: mAssignments, assignmentsWeight: mAssignmentsCost } =
        minWeightAssign(mWeightMatrix);
      const { assignments: aAssignments, assignmentsWeight: aAssignmentsCost } =
        minWeightAssign(aWeightMatrix);
      expect(aAssignments).toEqual(mAssignments);
      expect(aAssignmentsCost).toEqual(mAssignmentsCost);
    });

    it('should be able to support matrix (int32)', () => {
      const mWeightMatrix = {
        data: Int32Array.from([2, 1, 2, 1]),
        shape: [2, 2],
      };
      const aWeightMatrix = [
        [2, 1],
        [2, 1],
      ];
      const { assignments: mAssignments, assignmentsWeight: mAssignmentsCost } =
        minWeightAssign(mWeightMatrix);
      const { assignments: aAssignments, assignmentsWeight: aAssignmentsCost } =
        minWeightAssign(aWeightMatrix);
      expect(aAssignments).toEqual(mAssignments);
      expect(aAssignmentsCost).toEqual(mAssignmentsCost);
    });

    it('should be able to throw error when arrays input is invalid (invalid shape)', () => {
      const aWeightMatrix = [
        [1, 0],
        [0, 1, 1],
      ];
      expect(() => minWeightAssign(aWeightMatrix)).toThrowError();
    });

    it('should be able to throw error when arrays input is invalid (invalid weight)', () => {
      const aWeightMatrix = [
        [1, 0],
        [0, NaN],
      ];
      expect(() => minWeightAssign(aWeightMatrix)).toThrowError();
    });
  });

  describe('performance', () => {
    function createWeightMatrix(size: number) {
      const cost: number[][] = [];
      for (let i = 0; i < size; i++) {
        cost.push(Array.from({ length: size }, (_, j) => i * j));
      }
      return cost;
    }

    function createAssignments(size: number) {
      return Array.from({ length: size }, (_, i) => i).reverse();
    }

    describe('weightMatrix (5,5)', () => {
      const size = 5;
      let weightMatrix: number[][];
      let expectedAssignments: number[];
      beforeAll(() => {
        weightMatrix = createWeightMatrix(size);
        expectedAssignments = createAssignments(size);
      });
      it('should be able to compute assignments correctly', () => {
        const { assignments } = minWeightAssign(weightMatrix);
        expect(assignments).toEqual(expectedAssignments);
      });
    });

    describe('weightMatrix (50,50)', () => {
      const size = 50;
      let weightMatrix: number[][];
      let expectedAssignments: number[];
      beforeAll(() => {
        weightMatrix = createWeightMatrix(size);
        expectedAssignments = createAssignments(size);
      });
      it('should be able to compute assignments correctly', () => {
        const { assignments } = minWeightAssign(weightMatrix);
        expect(assignments).toEqual(expectedAssignments);
      });
    });

    describe('weightMatrix (500,500)', () => {
      const size = 500;
      let weightMatrix: number[][];
      let expectedAssignments: number[];
      beforeAll(() => {
        weightMatrix = createWeightMatrix(size);
        expectedAssignments = createAssignments(size);
      });
      it('should be able to compute assignments correctly', () => {
        const { assignments } = minWeightAssign(weightMatrix);
        expect(assignments).toEqual(expectedAssignments);
      });
    });
  });

  describe('precision issue', () => {
    it('should be able to compute assignments correctly. (Float32Array)', () => {
      const weightMatrix = {
        data: new Float32Array([
          Infinity,
          Infinity,
          Infinity,
          Infinity,
          Infinity,
          0.3967490792274475,
          0.20444045960903168,
          Infinity,
          Infinity,
        ]),
        shape: [3, 3],
      };
      const { assignments: minAssignments } = minWeightAssign(weightMatrix);
      const { assignments: maxAssignments } = maxWeightAssign(weightMatrix);
      expect(minAssignments).toEqual([null, 2, 0]);
      expect(maxAssignments).toEqual([0, 1, 2]);
    });
  });
});
