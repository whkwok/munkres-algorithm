// The following type aliases are used to increase readability.
type Row = number;
type Col = number;

// bigint and unsigned int TypedArray is not included
type SupportedTypedArray =
  | Int8Array
  | Int16Array
  | Int32Array
  | Float32Array
  | Float64Array;

type Constructor<T> = { new (...args: any[]): T };

export interface Matrix<T extends SupportedTypedArray> {
  /**
   * TypedArray storing the element of matrix in row-major order.
   */
  readonly data: T;
  /**
   * Shape of matrix.
   */
  readonly shape: readonly number[];
}

export interface AssignResult {
  /**
   * If col j is assigned to row i, then `assignments[i] = j`. If row i has no assignment, then `assignments[i] = null`.
   */
  readonly assignments: (number | null)[];
  /**
   * Sum of all weights in assignments.
   */
  readonly assignmentsWeight: number;
}

function arraysToMatrix(arrays: number[][]): Matrix<Float64Array> {
  const data = Float64Array.from(arrays.flat());
  const rowCount = arrays.length;
  const colCount = arrays[0]?.length ?? 0;
  if (data.length !== rowCount * colCount)
    throw new Error('Arrays cannot convert to a valid Matrix.');
  return { data, shape: [rowCount, colCount] };
}

function getNegativeWeightMatrix<T extends SupportedTypedArray>(
  weightMatrix: Matrix<T>
): Matrix<T> {
  const data = weightMatrix.data.slice() as T;
  for (let index = 0, size = data.length; index < size; index++) {
    data[index] = -data[index];
  }
  return { data, shape: [...weightMatrix.shape] };
}

function getTransposedData<T extends SupportedTypedArray>(
  data: T,
  rowCount: number,
  colCount: number
): T {
  const transposedData = data.slice() as T;
  let index = 0;
  for (let i = 0; i < rowCount; i++) {
    for (let j = 0; j < colCount; j++) {
      transposedData[j * rowCount + i] = data[index++];
    }
  }
  return transposedData;
}

/**
 * A runner to perform Munkres algorithm to solve minimum weight matching problem.
 */
class MunkresRunner<T extends SupportedTypedArray = Float64Array> {
  /**
   * Constructor of Typed Array
   */
  private TypedArray: Constructor<T>;
  /**
   * Number of row of weightMatrix (after transposed if needed)
   */
  private rowCount: number;
  /**
   * Number of col of weightMatrix (after transposed if needed)
   */
  private colCount: number;
  /**
   * Flag to indicate if the weightMatrix is transposed.
   */
  private transposed: boolean;
  /**
   * Flatten weights of edge. For edge (i, j), `edgeWeight = edgeWeights[i * colCount + j]`
   */
  private edgeWeights: T;
  /**
   * Flatten bounded weights of edge. For edge (i, j), `boundedEdgeWeight = edgeBoundedWeights[i * colCount + j]`
   */
  private edgeBoundedWeights!: T;
  /**
   * A non-tight lower bound for finite edgeWeights. Used to replace `-Infinity`.
   */
  private weightLowerBound!: number;
  /**
   * A non-tight upper bound for finite edgeWeights. Used to replace `Infinity`.
   */
  private weightUpperBound!: number;
  /**
   * A non-tight upper bound for slackValue.
   */
  private slackValueUpperBound!: number;
  /**
   * Labels of cols.
   */
  private colLabels: T;
  /**
   * Labels of rows.
   */
  private rowLabels: T;
  /**
   * Minimum slackValues between committed rows and each cols.
   */
  private candidateSlackValues: T;
  /**
   * Flags to indicated if rows are committed.
   */
  private rowCandidates: Uint8Array;
  /**
   * If row i and col j is matched, `colToRowMatches[j] = i`. If col j is unmatched, `colToRowMatches[j] = rowCount`.
   */
  private colToRowMatches: Uint32Array;
  /**
   * If row i and col j is matched, `rowToColMatches[i] = j`. If row i is unmatched, `rowToColMatches[i] = colCount`.
   */
  private rowToColMatches: Uint32Array;
  /**
   * If row i and col j is committed, `colToRowCommits[j] = i`. If col j is uncommitted, `colToRowCommits[j] = rowCount`.
   */
  private colToRowCommits: Uint32Array;
  /**
   * If row i and col j is a candidate, `colToRowCandidates[j] = i`.
   */
  private colToRowCandidates: Uint32Array;
  /**
   * Cache of assignments.
   */
  private _assignments: (number | null)[] | null;
  /**
   * Cache of assignmentsWeight.
   */
  private _assignmentsWeight: number | null;

  // Implementation Note:
  // For some reason, numerical operations involving Infinity is very slow (tested in NodeJs).
  // Replacing Infinity with finite number will speed up the runner quite significantly although it may reduces code readability.

  constructor(weightMatrix: Matrix<T>) {
    this.checkWeightMatrix(weightMatrix);
    let [rowCount, colCount] = weightMatrix.shape;
    if (rowCount <= colCount) {
      this.rowCount = rowCount;
      this.colCount = colCount;
      this.transposed = false;
      this.edgeWeights = weightMatrix.data as T;
    } else {
      // transpose the weightMatrix so that rowCount < colCount
      this.rowCount = colCount;
      this.colCount = rowCount;
      this.transposed = true;
      this.edgeWeights = getTransposedData(
        weightMatrix.data,
        rowCount,
        colCount
      );
      rowCount = this.rowCount;
      colCount = this.colCount;
    }
    const TypedArray = (this.edgeWeights as any).constructor as Constructor<T>;
    this.TypedArray = TypedArray;
    this.rowLabels = new TypedArray(rowCount);
    this.colLabels = new TypedArray(colCount);
    this.candidateSlackValues = new TypedArray(colCount);
    this.rowCandidates = new Uint8Array(rowCount);
    this.colToRowMatches = new Uint32Array(colCount).fill(rowCount);
    this.rowToColMatches = new Uint32Array(rowCount).fill(colCount);
    this.colToRowCommits = new Uint32Array(colCount).fill(rowCount);
    this.colToRowCandidates = new Uint32Array(colCount);
    this.setBounds();
    this._assignments = null;
    this._assignmentsWeight = null;
  }

  run(): void {
    const { rowCount } = this;
    this.initRowLabels();
    this.initMatching();
    // try to increase matching until no unmatched row found.
    let unmatchedRow;
    while ((unmatchedRow = this.findUnmatchedRow()) !== rowCount) {
      this.initCommitsAndCandidates(unmatchedRow);
      this.increaseMatching();
    }
  }

  getAssignments(): (number | null)[] {
    if (this._assignments === null) {
      let fromCount, toCount, matches, getEdge;
      const { rowCount, colCount, weightUpperBound, edgeBoundedWeights } = this;
      if (this.transposed) {
        fromCount = colCount;
        toCount = rowCount;
        matches = this.colToRowMatches;
        getEdge = (from: number, to: number) => to * colCount + from;
      } else {
        fromCount = rowCount;
        toCount = colCount;
        matches = this.rowToColMatches;
        getEdge = (from: number, to: number) => from * colCount + to;
      }
      const assignments: (number | null)[] = new Array(fromCount).fill(null);
      for (let from = 0; from < fromCount; from++) {
        const to = matches[from];
        if (
          to !== toCount &&
          edgeBoundedWeights[getEdge(from, to)] < weightUpperBound
        )
          assignments[from] = to;
      }
      this._assignments = assignments;
    }
    return this._assignments;
  }

  getAssignmentsWeight(): number {
    if (this._assignmentsWeight === null) {
      const {
        rowCount,
        colCount,
        weightUpperBound,
        rowToColMatches,
        edgeWeights,
      } = this;
      let assignmentsWeight = 0;
      for (let i = 0; i < rowCount; i++) {
        const j = rowToColMatches[i];
        const edgeWeight = edgeWeights[i * colCount + j];
        if (j !== colCount && edgeWeight < weightUpperBound)
          assignmentsWeight += edgeWeight;
      }
      this._assignmentsWeight = assignmentsWeight;
    }
    return this._assignmentsWeight;
  }

  private match(row: Row, col: Col): void {
    this.rowToColMatches[row] = col;
    this.colToRowMatches[col] = row;
  }

  private checkWeightMatrix(weightMatrix: Matrix<T>): void {
    const { data, shape } = weightMatrix;
    if (shape.length !== 2)
      throw new Error('weightMatrix must be two dimensional.');
    for (let index = 0, size = data.length; index < size; index++) {
      if (Number.isNaN(data[index]))
        throw new Error('NaN weight is not allowed.');
    }
  }

  private forcePrecision(n: number) {
    return new this.TypedArray([n])[0];
  }

  private setBounds(): void {
    const { edgeWeights } = this;
    let minEdgeWeight = Infinity;
    let maxEdgeWeight = -Infinity;
    let weightsAllFinite = true;
    for (let edge = 0, size = edgeWeights.length; edge < size; edge++) {
      const edgeWeight = edgeWeights[edge];
      if (!Number.isFinite(edgeWeight)) {
        weightsAllFinite = false;
        continue;
      }
      if (edgeWeight < minEdgeWeight) minEdgeWeight = edgeWeight;
      if (edgeWeight > maxEdgeWeight) maxEdgeWeight = edgeWeight;
    }

    if (minEdgeWeight === Infinity) minEdgeWeight = 0; // only happens when all weights are not finite.
    if (maxEdgeWeight === -Infinity) maxEdgeWeight = 0; // only happens when all weights are not finite.
    const range = (maxEdgeWeight - minEdgeWeight) * this.rowCount;
    // Need Confirm: Is this enough for a lower bound ?
    const weightLowerBound = minEdgeWeight - range - 1;
    // Need Confirm: Is this enough for a upper bound ?
    const weightUpperBound = maxEdgeWeight + range + 1;
    // Important: Default number in JS may have higher precision than edgeWeight data type.
    // Without forcing the precision of bounds to be same as edgeWeight data type, the bounds may become invalid.
    this.weightLowerBound = this.forcePrecision(weightLowerBound);
    this.weightUpperBound = this.forcePrecision(weightUpperBound);
    this.slackValueUpperBound = this.forcePrecision(
      weightUpperBound - weightLowerBound
    );
    if (weightsAllFinite) {
      this.edgeBoundedWeights = this.edgeWeights;
    } else {
      const edgeBoundedWeights = this.edgeWeights.slice() as T;
      for (let edge = 0, size = edgeWeights.length; edge < size; edge++) {
        const edgeWeight = edgeWeights[edge];
        if (edgeWeight < weightLowerBound)
          edgeBoundedWeights[edge] = weightLowerBound;
        else if (edgeWeight > weightUpperBound)
          edgeBoundedWeights[edge] = weightUpperBound;
      }
      this.edgeBoundedWeights = edgeBoundedWeights;
    }
  }

  /**
   * Initialize rowLabels by the minimum edge weight.
   */
  private initRowLabels(): void {
    const {
      rowLabels,
      weightLowerBound,
      weightUpperBound,
      edgeBoundedWeights,
    } = this;
    let rowLabel = weightUpperBound;
    for (let edge = 0, size = edgeBoundedWeights.length; edge < size; edge++) {
      const edgeWeight = edgeBoundedWeights[edge];
      if (edgeWeight < rowLabel) rowLabel = edgeWeight;
      if (rowLabel === weightLowerBound) break;
    }
    rowLabels.fill(rowLabel);
  }

  /**
   * Initialize matching by greedy matching.
   */
  private initMatching(): void {
    const {
      rowCount,
      colCount,
      weightUpperBound,
      rowLabels,
      edgeBoundedWeights,
      colToRowMatches,
    } = this;
    const rowLabel = rowLabels[0] ?? weightUpperBound; // all row labels are the same at this stage.
    if (rowLabel === weightUpperBound) return; // no possible matching if all edge weights are Infinity.
    for (let i = 0; i < rowCount; i++) {
      let edge = i * colCount;
      for (let j = 0; j < colCount; j++) {
        if (colToRowMatches[j] === rowCount) {
          // j is unmatched
          // colLabel is 0 at this stage so slackValue = edgeWeight - rowLabel
          if (edgeBoundedWeights[edge] === rowLabel) {
            // slackValue is 0
            this.match(i, j);
            break;
          }
        }
        edge++;
      }
    }
  }

  /**
   * Set a unmatched row as candidate.
   * Set candidatesSlackValue of col j to the slackValue between the candidate row and col j.
   */
  private initCommitsAndCandidates(row: Row): void {
    const {
      rowCount,
      colCount,
      rowCandidates,
      rowLabels,
      colLabels,
      edgeBoundedWeights,
      candidateSlackValues,
      colToRowCommits,
      colToRowCandidates,
    } = this;
    colToRowCommits.fill(rowCount); // reset commits
    rowCandidates.fill(0);
    rowCandidates[row] = 1;
    colToRowCandidates.fill(row);
    const rowLabel = rowLabels[row];
    let edge = row * colCount;
    for (let j = 0; j < colCount; j++) {
      candidateSlackValues[j] =
        edgeBoundedWeights[edge++] - rowLabel - colLabels[j];
    }
  }

  private findUnmatchedRow(): Row {
    const { rowCount, colCount, rowToColMatches } = this;
    for (let i = 0; i < rowCount; i++) {
      if (rowToColMatches[i] === colCount) return i;
    }
    return rowCount;
  }

  /**
   * Main logic of the algorithm.
   *
   * Concept:
   * - Increase matching by augmenting a path that alternates between committed edge and matched edge.
   * - An edge can only be committed if its slack value is zero.
   * - Throughout the path searching process, if there is no zero-slack edge, update labels to make one.
   * - The edge to be converted to a zero-slack edge is the min-slack edge at current stage.
   */
  private increaseMatching(): void {
    const { rowCount, colToRowMatches } = this;
    let allowedLoopCount = rowCount; // for preventing infinite loop.
    while (allowedLoopCount) {
      allowedLoopCount--;
      const [minSlackValue, minSlackRow, minSlackCol] = this.findMinSlack();
      if (minSlackRow === -1) throw new Error('There must be a minSlack.'); // should not happen
      if (minSlackValue > 0) this.updateLabels(minSlackValue);
      const matchedRow = colToRowMatches[minSlackCol];
      if (matchedRow === rowCount) {
        // minSlackCol is unmatched
        this.augmentAlternatePath(minSlackRow, minSlackCol); // follow and augment alternate path.
        return;
      }
      // minSlackCol is matched
      this.updateCommitsAndCandidates(
        minSlackValue,
        minSlackRow,
        minSlackCol,
        matchedRow
      );
    }
    throw new Error('Number of loops exceeds rowCount.'); // should not happen
  }

  private findMinSlack(): [number, Row, Col] {
    const {
      rowCount,
      colCount,
      slackValueUpperBound,
      candidateSlackValues,
      colToRowCommits,
      colToRowCandidates,
    } = this;
    let minSlackValue = slackValueUpperBound;
    let minSlackRow = -1;
    let minSlackCol = -1;
    for (let j = 0; j < colCount; j++) {
      if (colToRowCommits[j] !== rowCount) continue; // skip committed col
      const candidateSlackValue = candidateSlackValues[j];
      if (candidateSlackValue < minSlackValue) {
        minSlackValue = candidateSlackValue;
        minSlackRow = colToRowCandidates[j];
        minSlackCol = j;
      }
      if (minSlackValue === 0) break; // shortcut here as we know that slack must be >= 0.
    }
    return [minSlackValue, minSlackRow, minSlackCol];
  }

  private updateLabels(minSlackValue: number): void {
    const {
      rowCount,
      colCount,
      rowCandidates,
      rowLabels,
      colLabels,
      colToRowCommits,
    } = this;
    for (let i = 0; i < rowCount; i++) {
      if (rowCandidates[i]) rowLabels[i] += minSlackValue;
    }
    for (let j = 0; j < colCount; j++) {
      if (colToRowCommits[j] !== rowCount) colLabels[j] -= minSlackValue;
    }
  }

  private augmentAlternatePath(initRow: Row, initCol: Col): void {
    const { rowCount, colCount, rowToColMatches, colToRowCommits } = this;
    let committedRow = initRow;
    let committedCol = initCol;
    let allowedLoopCount = rowCount; // for preventing infinite loop.
    while (allowedLoopCount) {
      allowedLoopCount--;
      const matchedCol = rowToColMatches[committedRow];
      this.match(committedRow, committedCol);
      committedCol = matchedCol;
      if (committedCol === colCount) return; // if no matched row is found, the path completes.
      committedRow = colToRowCommits[committedCol];
      if (committedRow === rowCount)
        throw new Error(
          'There must be an edge between from col to row in commits.'
        ); // should not happen.
    }
    throw new Error('Number of loops exceeds rowCount.'); // should not happen.
  }

  private updateCommitsAndCandidates(
    minSlackValue: number,
    minSlackRow: Row,
    minSlackCol: Col,
    matchedRow: Row
  ): void {
    const {
      rowCount,
      colCount,
      rowCandidates,
      rowLabels,
      colLabels,
      candidateSlackValues,
      edgeBoundedWeights,
      colToRowCommits,
      colToRowCandidates,
    } = this;
    // commit minSlackRow and minSlackCol
    colToRowCommits[minSlackCol] = minSlackRow;
    // set matchedRow as candidate
    rowCandidates[matchedRow] = 1;
    // update candidateSlackValues as a new row become candidate.
    candidateSlackValues[minSlackCol] = 0; // slackValue become 0 after updating labels.
    const matchedRowLabel = rowLabels[matchedRow];
    let edge = matchedRow * colCount;
    for (let j = 0; j < colCount; j++) {
      if (colToRowCommits[j] === rowCount) {
        // skip committed col as committed col must have a zero-slack edge to a committed row
        const currentSlackValue = candidateSlackValues[j] - minSlackValue; // slackValue is reduced by minSlackValue after updating labels.
        const newSlackValue =
          edgeBoundedWeights[edge] - matchedRowLabel - colLabels[j];
        if (newSlackValue < currentSlackValue) {
          candidateSlackValues[j] = newSlackValue;
          colToRowCandidates[j] = matchedRow;
        } else candidateSlackValues[j] = currentSlackValue;
      }
      edge++;
    }
  }
}

export function minWeightAssign(
  weightMatrix: Matrix<SupportedTypedArray> | number[][]
): AssignResult {
  if (weightMatrix instanceof Array)
    weightMatrix = arraysToMatrix(weightMatrix);
  const runner = new MunkresRunner(weightMatrix);
  runner.run();
  return {
    get assignments() {
      return runner.getAssignments(); // lazily get assignments
    },
    get assignmentsWeight() {
      return runner.getAssignmentsWeight(); // lazily get assignmentsCost
    },
  };
}

export function maxWeightAssign(
  weightMatrix: Matrix<SupportedTypedArray> | number[][]
): AssignResult {
  if (weightMatrix instanceof Array)
    weightMatrix = arraysToMatrix(weightMatrix);
  weightMatrix = getNegativeWeightMatrix(weightMatrix);
  const runner = new MunkresRunner(weightMatrix);
  runner.run();
  return {
    get assignments() {
      return runner.getAssignments(); // lazily get assignments
    },
    get assignmentsWeight() {
      return 0 - runner.getAssignmentsWeight(); // lazily get assignmentsCost. negation here as we negated weightMatrix before.
    },
  };
}
