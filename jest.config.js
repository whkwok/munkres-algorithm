module.exports = {
  clearMocks: true,
  moduleFileExtensions: ['js', 'ts', 'json'],
  testMatch: ['**/*.spec.+(ts|js)'],
  moduleNameMapper: {
    'src(/?.*)': '<rootDir>/src$1',
  },
  transform: { '^.+\\.(t|j)s$': 'ts-jest' },
};
